"""mario URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from pdb import set_trace

import django.contrib.auth.models
import django.contrib.auth.views
import django.views.generic
import django.views.generic.detail
from django.conf.urls import include, url
from django.contrib import admin
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.urlresolvers import reverse
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from rest_framework import permissions, routers, serializers, viewsets
from rest_framework.authtoken import views
from guardian.shortcuts import assign_perm, get_objects_for_user
from loan.models import Loan


class LoanSerializer(serializers.ModelSerializer):
    class Meta:
        model = Loan
        fields = ('created', 'company', 'sector', 'synopsis', 'owner')

class UserSerializer(serializers.ModelSerializer):
    loans = serializers.PrimaryKeyRelatedField(many=True, queryset=Loan.objects.all())
    class Meta:
        model = django.contrib.auth.models.User
        fields = ('id', 'username', 'loans')

class UserViewSet(viewsets.ReadOnlyModelViewSet):
    """
    This viewset automatically provides `list` and `detail` actions.
    """
    queryset = django.contrib.auth.models.User.objects.all()
    serializer_class = UserSerializer

class IsOwnerOrReadOnly(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        if request.method in ['OPTIONS']:
            return True
        return obj.owner == request.user


class LoanViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list`, `create`, `retrieve`,
    `update` and `destroy` actions.

    Additionally we also provide an extra `highlight` action.
    """
    def get_queryset(self):
        user = self.request.user
        return get_objects_for_user(user, 'loan.view_loan')

    serializer_class = LoanSerializer
    permission_classes = (
        permissions.IsAuthenticated,
        IsOwnerOrReadOnly,)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

class Login(LoginRequiredMixin):
    login_url = '/login/'
    redirect_field_name = 'redirect_to'

class LoanListView(Login, django.views.generic.ListView):
    template_name = 'loan_list.html'
    def get_queryset(self):
        return get_objects_for_user(self.request.user, 'loan.view_loan')

    def get_context_data(self, **kwargs):
            context = super(LoanListView, self).get_context_data(**kwargs)
            loans = list(context['object_list'])
            for loan in loans:
                loan.own = self.request.user == loan.owner
            context['loans'] = loans
            return context

class LoanDetailView(Login, django.views.View):
    def get(self, request, pk):
        loan = Loan.objects.get(pk=pk)
        if not self.request.user.has_perm('loan.view_loan', loan):
            return HttpResponse('Unauthorized', status=401)
        return render(request, 'loan_detail.html', {'object': loan})

class LoanForm(django.forms.ModelForm):
    class Meta:
        model = Loan
        fields = ['company', 'sector', 'synopsis']

class LoanDetailEditView(Login, django.views.View):
    # django.views.generic.detail.DetailView):
    template_name = 'loan_detail_edit.html'
    def post(self, request, pk):
        loan = Loan.objects.get(pk=pk)
        if loan.owner != request.user:
            return HttpResponse('Unauthorized', status=401)
        form = LoanForm(request.POST, instance=loan)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('loan_all'))

    def get(self, request, pk):
        loan = Loan.objects.get(pk=pk)
        if loan.owner != request.user:
            return HttpResponse('Unauthorized', status=401)
        form = LoanForm(instance=loan)
        return render(request, self.template_name, {'form': form})


class LoanDetailAddView(Login, django.views.View):
    template_name = 'loan_detail_edit.html'
    login_url = '/login/'
    redirect_field_name = 'redirect_to'
    def get(self, request):
        form = LoanForm()
        return render(request, self.template_name, {'form': form})

    def post(self, request):
        form = LoanForm(request.POST)
        if form.is_valid():
            loan = form.save(commit=False)
            loan.owner = request.user
            loan.save()
            assign_perm('loan.view_loan', request.user, loan)
            return HttpResponseRedirect(reverse('loan_all')) # 'A loan with id={0} was created'.format(loan))
        return render(request, self.template_name, {'form': form})





router = routers.DefaultRouter()
router.register(r'loan', LoanViewSet, 'loansofuser')
router.register(r'users', UserViewSet)
urlpatterns = [
    url('^$/', django.views.generic.TemplateView.as_view(template_name='root.html'), name='root'),
    url('^loan/all/', LoanListView.as_view(), name='loan_all'),
    url('^loan/add/', LoanDetailAddView.as_view(), name='loan_add'),
    url('^loan/detail/(?P<pk>[0-9]+)/$', LoanDetailView.as_view(), name='loan_detail'),
    url('^loan/detail/(?P<pk>[0-9]+)/edit/', LoanDetailEditView.as_view(), name='loan_detail_edit'),
    url('^', include('django.contrib.auth.urls')),
    #url('^home/', django.views.generic.TemplateView.as_view(template_name='home.html'), name='home'),
    #url('^home/', django.views.generic.TemplateView.as_view(template_name='home.html'), name='home'),
    #url(r'^login/$', django.contrib.auth.views.login, name='login'),
    #url(r'^logout/$', django.contrib.auth.views.logout, name='logout'),
    #url(r'^admin/', admin.site.urls),
    url(r'^api/', include(router.urls)),
    url(r'^admin/', admin.site.urls),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^api-token-auth/', views.obtain_auth_token),
]
