This is the solution by Mario Orne Diaz Anadon, ornediaz@gmail.com, 07942 363156

I am using Django 1.11.1

I obtain object-level permissions by using the django guardian library.

As far as I can tell the solution works.

I have added some test on loan/tests.py. More tests would be recommended,
particularly for the web part.

Most of the code is urls.py and in production should be moved to views,
serializers, etc.


==================================
Web page for users

The main page for users is  http://127.0.0.1:8000/loan/all/

==================================
Admin page for users

http://127.0.0.1:8000/admin/

An admin can add view permissions for a user and loan following these steps
- Loan
- loan detail
- object permissions
- enter 'user identification' and click  manage user
- add 'View loan' and save.


==================================
API


An authorization token can be obtained by running:

http POST 127.0.0.1:8000/api-token-auth/ username=admin1 password=contrasena1

The above might return:
{
    "token": "7f5127166e51c761b8e8d4d743300425e7e4ce64"
}


Thereafter, in order to use the API, you can use:

 http GET 127.0.0.1:8000/loan/ 'Authorization: Token d328dacd9c47cec0985f30a6de95a32978e6f7ab'
