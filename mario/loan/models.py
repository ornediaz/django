# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from pdb import set_trace

from django.conf import settings
from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.encoding import python_2_unicode_compatible
from guardian.shortcuts import assign_perm
from rest_framework.authtoken.models import Token


@receiver(post_save, sender=User)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)
@python_2_unicode_compatible
class Loan(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    company = models.CharField(max_length=100, blank=True, default='')
    sector = models.CharField(max_length=100, blank=True, default='')
    synopsis = models.CharField(max_length=100, blank=True, default='')
    owner = models.ForeignKey('auth.User', related_name='snippets', on_delete=models.CASCADE)
    def save(self, *args, **kwargs):
        super(Loan, self).save(*args, **kwargs)
        # assign_perm('loan.view_loan', self.owner, self)

    def __str__(self):
        return '{0}-{1}'.format(self.id, self.company)

    class Meta:
        permissions = (
            ('view_loan', 'View loan'),
        )
        ordering = ('created',)
