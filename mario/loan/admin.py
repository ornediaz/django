# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from loan.models import Loan

from guardian.admin import GuardedModelAdmin


class LoanAdmin(GuardedModelAdmin):
    #prepopulated_fields = {"area": ("sector",)}
    list_display = ('company', 'sector', 'created')
    search_fields = ('company', 'owner')
    ordering = ('-created',)
    date_hierarchy = 'created'

admin.site.register(Loan, LoanAdmin)
