# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.test import TestCase
from loan.models import Loan
from rest_framework.test import APIClient
from rest_framework.test import APITestCase
from django.contrib.auth.models import User
from freezegun import freeze_time
from guardian.shortcuts import assign_perm, get_objects_for_user
from pdb import set_trace
from django.test import Client

# Create your tests here.
@freeze_time('2012-01-03')
class LoanApiTestCase(TestCase):
    def setUp(self):
        self.user1 = User.objects.create_user('user1', 'user1@thebeatles.com', 'contrasena1')
        self.user2 = User.objects.create_user('user2', 'user1@thebeatles.com', 'contrasena2')
        self.user3 = User.objects.create_user('user3', 'user3@thebeatles.com', 'contrasena3')
        self.client = APIClient()
        self.loan1 = Loan.objects.create(company='Alsa', owner=self.user1)
        assign_perm('loan.view_loan', self.user1, self.loan1)
        self.loan2 = Loan.objects.create(company='c2', owner=self.user1)
        assign_perm('loan.view_loan', self.user1, self.loan2)
        self.loan3 = Loan.objects.create(company='c3', owner=self.user2)
        assign_perm('loan.view_loan', self.user2, self.loan3)
        assign_perm('loan.view_loan', self.user3, self.loan1)
        self.loan_data = [
            dict([('created', u'2012-01-03T00:00:00Z'), ('company', u'Alsa'), ('sector', u''), ('synopsis', u''), ('owner', self.user1.id)]),
            dict([('created', u'2012-01-03T00:00:00Z'), ('company', u'c2'), ('sector', u''), ('synopsis', u''), ('owner', self.user1.id)]),
            dict([('created', u'2012-01-03T00:00:00Z'), ('company', u'c3'), ('sector', u''), ('synopsis', u''), ('owner', self.user2.id)]),
        ]

    def test_unauthenticated_post(self):
        response = self.client.post('/api/loan/', format='json')
        self.assertEqual(
            response.data,
            {"detail":"Authentication credentials were not provided."}
        )
    def test_list_get_unauthenticated(self):
        response = self.client.get('/api/loan/', format='json')
        self.assertEqual(
            response.data,
            {"detail":"Authentication credentials were not provided."}
        )
    def test_list_get_user1(self):
        self.client.force_authenticate(self.user1)
        response = self.client.get('/api/loan/', format='json')
        self.assertEqual(
            response.data,
            self.loan_data[:2]
        )

    def test_list_get_user2(self):
        self.client.force_authenticate(self.user2)
        response = self.client.get('/api/loan/', format='json')
        self.assertEqual(
            response.data,
            self.loan_data[2:3]
        )
    def test_list_get_user3(self):
        self.client.force_authenticate(self.user3)
        response = self.client.get('/api/loan/', format='json')
        self.assertEqual(
            response.data,
            self.loan_data[:1]
        )

    def test_detail_get_unauthenticated(self):
        response = self.client.get(
            '/api/loan/{0}/'.format(self.loan1.id),
            format='json'
        )
        self.assertEqual(
            response.data,
            {"detail":"Authentication credentials were not provided."}
        )
    def test_detail_get_user1_b(self):
        self.client.force_authenticate(self.user1)
        response = self.client.get(
            '/api/loan/{0}/'.format(self.loan1.id),
            format='json'
        )
        self.assertEqual(
            response.data,
            self.loan_data[0]
        )
    def test_detail_get_user1_c(self):
        self.client.force_authenticate(self.user1)
        response = self.client.get(
            '/api/loan/{0}/'.format(self.loan2.id),
            format='json'
        )
        self.assertEqual(
            response.data,
            self.loan_data[1]
        )
    def test_detail_get_user1_c(self):
        self.client.force_authenticate(self.user1)
        response = self.client.get(
            '/api/loan/{0}/'.format(self.loan3.id),
            format='json'
        )
        self.assertEqual(
            response.data,
            {u'detail': u'Not found.'}
        )
    def test_detail_get_user2_a(self):
        self.client.force_authenticate(self.user2)
        response = self.client.get(
            '/api/loan/{0}/'.format(self.loan1.id),
            format='json'
        )
        self.assertEqual(
            response.data,
            {u'detail': u'Not found.'}
        )
    def test_detail_get_user2_b(self):
        self.client.force_authenticate(self.user2)
        response = self.client.get(
            '/api/loan/{0}/'.format(self.loan3.id),
            format='json'
        )
        self.assertEqual(
            response.data,
            self.loan_data[2]
        )
    def test_detail_get(self):
        assign_perm('view_loan', self.user2, self.loan1)
        self.client.force_authenticate(self.user2)
        response = self.client.get(
            '/api/loan/{0}/'.format(self.loan2.id),
            format='json'
        )
        # self.assertEqual(
        #     response.data,
        #     self.loan_data[1]
        # )

    def test6(self):
        self.assertFalse(self.user1.has_perm('add_loan', self.loan1))

        self.assertTrue(self.user1.has_perm('loan.view_loan', self.loan1))
        self.assertTrue(self.user1.has_perm('loan.view_loan', self.loan1))
        self.assertFalse(self.user1.has_perm('loan.view_loan', self.loan3))
        self.assertEqual(
            list(get_objects_for_user(self.user1, 'loan.view_loan', Loan)),
            [self.loan1, self.loan2]
        )

        self.assertFalse(self.user2.has_perm('loan.view_loan', self.loan1))
        self.assertFalse(self.user2.has_perm('loan.view_loan', self.loan2))
        self.assertTrue(self.user2.has_perm('loan.view_loan', self.loan3))
        self.assertEqual(
            list(get_objects_for_user(self.user2, 'loan.view_loan', Loan)),
            [self.loan3]
        )

        self.assertTrue(self.user3.has_perm('loan.view_loan', self.loan1))
        self.assertFalse(self.user3.has_perm('view_loan', self.loan2))
        self.assertFalse(self.user3.has_perm('view_loan', self.loan3))
        self.assertEqual(
            list(get_objects_for_user(self.user3, 'loan.view_loan', Loan)),
            [self.loan1]
        )
    def test7(self):
        assign_perm('view_loan', self.user1, self.loan1)
        self.assertFalse(self.user1.has_perm('add_loan', self.loan1))
        self.assertTrue(self.user1.has_perm('view_loan', self.loan1))


class LoanWebTestCase(TestCase):
    def setUp(self):
        self.user1 = User.objects.create_user('user1', 'user1@thebeatles.com', 'contrasena1')
        self.user2 = User.objects.create_user('user2', 'user1@thebeatles.com', 'contrasena2')
        self.user3 = User.objects.create_user('user3', 'user3@thebeatles.com', 'contrasena3')
        self.client = Client()
        self.loan1 = Loan.objects.create(company='Alsa', owner=self.user1)
        assign_perm('loan.view_loan', self.user1, self.loan1)
        assign_perm('loan.view_loan', self.user2, self.loan1)
        self.loan2 = Loan.objects.create(company='c2', owner=self.user1)
        assign_perm('loan.view_loan', self.user1, self.loan2)
        self.loan3 = Loan.objects.create(company='c3', owner=self.user2)
        assign_perm('loan.view_loan', self.user2, self.loan3)
        self.loan_data = [
            dict([('created', u'2012-01-03T00:00:00Z'), ('company', u'Alsa'), ('sector', u''), ('synopsis', u''), ('owner', self.user1.id)]),
            dict([('created', u'2012-01-03T00:00:00Z'), ('company', u'c2'), ('sector', u''), ('synopsis', u''), ('owner', self.user1.id)]),
            dict([('created', u'2012-01-03T00:00:00Z'), ('company', u'c3'), ('sector', u''), ('synopsis', u''), ('owner', self.user2.id)]),
        ]
    def testRedirection1(self):
        response = self.client.get('/loan/all/')
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['Location'], '/login/?redirect_to=/loan/all/')
    def testRedirection2(self):
        response = self.client.get('/loan/add/')
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['Location'], '/login/?redirect_to=/loan/add/')
    def testRedirection3(self):
        response = self.client.get('/loan/detail/1/')
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['Location'], '/login/?redirect_to=/loan/detail/1/')
    def testRedirection4(self):
        response = self.client.get('/loan/detail/1/edit/')
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['Location'], '/login/?redirect_to=/loan/detail/1/edit/')
    def test_can_load_page_all(self):
        self.client.force_login(self.user1)
        response = self.client.get('/loan/all/')
        self.assertEqual(response.status_code, 200)
    def test_can_load_page_detail(self):
        self.client.force_login(self.user1)
        response = self.client.get('/loan/detail/1/')
        self.assertEqual(response.status_code, 200)
    def test_can_load_page_detail_edit(self):
        self.client.force_login(self.user1)
        response = self.client.get('/loan/detail/1/edit/')
        self.assertEqual(response.status_code, 200)
    def test_can_load_page_add(self):
        self.client.force_login(self.user1)
        response = self.client.get('/loan/add/')
        self.assertEqual(response.status_code, 200)
    def test_can_load_page_user2_all(self):
        self.client.force_login(self.user2)
        response = self.client.get('/loan/all/')
        self.assertEqual(response.status_code, 200)
    def test_can_load_page_user2_detail(self):
        self.client.force_login(self.user2)
        response = self.client.get('/loan/detail/1/')
        self.assertEqual(response.status_code, 200)
    def test_can_load_page_user2_detail_edit(self):
        self.client.force_login(self.user2)
        response = self.client.get('/loan/detail/1/edit/')
        self.assertEqual(response.status_code, 401)
    def test_can_load_page_user2_add(self):
        self.client.force_login(self.user2)
        response = self.client.get('/loan/add/')
        self.assertEqual(response.status_code, 200)
    def test_can_load_page_user3_all(self):
        self.client.force_login(self.user3)
        response = self.client.get('/loan/all/')
        self.assertEqual(response.status_code, 200)
    def test_can_load_page_user3_detail(self):
        self.client.force_login(self.user3)
        response = self.client.get('/loan/detail/1/')
        self.assertEqual(response.status_code, 401)
    def test_can_load_page_user3_detail_edit(self):
        self.client.force_login(self.user3)
        response = self.client.get('/loan/detail/1/edit/')
        self.assertEqual(response.status_code, 401)
    def test_can_load_page_user3_add(self):
        self.client.force_login(self.user3)
        response = self.client.get('/loan/add/')
        self.assertEqual(response.status_code, 200)
