from django.contrib.auth.models import User

class UserSerializer(serializers.ModelSerializer):
    loans = serializers.PrimaryKeyRelatedField(many=True, queryset=Loan.objects.all())
    class Meta:
        model = User
        fields = ('id', 'username', 'loans')
